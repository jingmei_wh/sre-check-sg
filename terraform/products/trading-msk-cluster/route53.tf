resource "aws_route53_record" "DNS_record_br" {
  count   = var.number_of_broker_nodes

  zone_id = data.terraform_remote_state.infra.outputs.route53_zone_id
  name    = "${var.product}-${format("br%01d", count.index + 1)}.${data.terraform_remote_state.infra.outputs.route53_zone_name}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${format("b-%01d", count.index + 1)}${local.broker_endpoint_suffix}"]
}


resource "aws_route53_record" "DNS_record_zo" {
  count   = var.number_of_broker_nodes

  zone_id = data.terraform_remote_state.infra.outputs.route53_zone_id
  name    = "${var.product}-${format("zo%01d", count.index + 1)}.${data.terraform_remote_state.infra.outputs.route53_zone_name}"
  type    = "CNAME"
  ttl     = "300"
  records = ["${format("z-%01d", count.index + 1)}${local.zookeeper_endpoint_suffix}"]
}
