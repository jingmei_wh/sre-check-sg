/**
Infra remote state which is used for getting VPC related information.
- For example a private subnet id used in this infrastructure can be found in
the infra remote state.
- Another example is the common security group ids which are used for most of the
EC2 instances.
It is State which holds the expected infrastructure data needed for EC2 instaces to be
integrated.
 */

# This is managed by AWS CoE team
data "terraform_remote_state" "infra" {
  backend = "s3"

  config = {
    bucket = "williamhill-${var.channel}-infra-${var.environment}-${data.aws_region.current.name}-tfstate"
    key    = "${var.environment}-common.tfstate"
    region = data.aws_region.current.name
  }
}

# This is Tradings managed remote_statea
data "terraform_remote_state" "infrastructure" {
   backend = "s3"

   config = {
     bucket = "wh-${var.channel}-infrastructure-${var.environment}-${data.aws_region.current.name}-tfstate"
     key    = "${var.environment}-common.tfstate"
     region = data.aws_region.current.name
   }
}
