## Overview

Common stack contains all common resources that should be applied in every region. <br>
This template will create MSK and security group


## Usage

REGION=`eu-west-1` ENV=`<DEV>` VERSION=`<VERSION>` aws-okta exec `<channel-account>` -- make onetime <br>

### Under specific path of environment

REGION=`eu-west-1` ENV=`<DEV>` VERSION=`<VERSION>` aws-okta exec master -- terragrunt plan <br>
REGION=`eu-west-1` ENV=`<DEV>` VERSION=`<VERSION>` aws-okta exec master -- terragrunt apply <br>

## Intro

 Creates an MSK cluster.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| channel |  | string | n/a | yes |
| environment |  | string | n/a | yes |
| msk\_cluster\_name |  | string | `""` | no |
| org |  | string | n/a | yes |
| region |  | string | n/a | yes |
| tags |  | map | `<map>` | no |
| gitlab_url |  | string | n/a | yes |
| product_version |  | string | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| bootstrap\_brokers | Plaintext connection host:port pairs |
| bootstrap\_brokers\_tls | TLS connection host:port pairs |
| msk\_cluster\_arn | Amazon Resource Name (ARN) of the MSK cluster |
| zookeeper\_connect\_string | A comma separated list of one or more IP:port pairs to use to connect to the Apache Zookeeper cluster |
