resource "aws_security_group" "MSK-SG" {
  name        = var.stack
  description = "${var.product} SG"
  vpc_id      = data.terraform_remote_state.infra.outputs.vpc_id

  tags = merge(
    module.tags.map,
    { Name = "${var.product}-${var.channel}" }
  )
}

resource "aws_security_group_rule" "inbound_to_communicate_with_prometheus" {
  description       = "Rule which allows prometheus to connect to kafka"
  type              = "ingress"
  from_port         = 11001
  to_port           = 11002
  protocol          = "tcp"
  cidr_blocks       = flatten([data.terraform_remote_state.infra.outputs.vpc_private_subnets_cidr_blocks, module.tf-common-network-blocks.office_ip_cidrs])
  security_group_id = aws_security_group.MSK-SG.id
}

resource "aws_security_group_rule" "inbound_to_communicate_with_msk_cluster" {
  description = "Rule which allows AWS+office to connect to kafka"
  type        = "ingress"
  from_port   = 9092
  to_port     = 9092
  protocol    = "tcp"

  cidr_blocks       = flatten([data.terraform_remote_state.infra.outputs.vpc_private_subnets_cidr_blocks, module.tf-common-network-blocks.office_ip_cidrs, module.tf-common-network-blocks.vpn_ip_cidrs])
  security_group_id = aws_security_group.MSK-SG.id
}

resource "aws_security_group_rule" "inbound_to_communicate_with_msk_zookeeper" {
  description = "Rule which allows AWS+office to connect to zookeeper"
  type        = "ingress"
  from_port   = 2181
  to_port     = 2181
  protocol    = "tcp"

  cidr_blocks       = flatten([data.terraform_remote_state.infra.outputs.vpc_private_subnets_cidr_blocks, module.tf-common-network-blocks.office_ip_cidrs, module.tf-common-network-blocks.vpn_ip_cidrs])
  security_group_id = aws_security_group.MSK-SG.id
}

resource "aws_security_group_rule" "outbound_to_communicate_with_msk_cluster_from_real_trading" {
  description = "Rule which allows real-common-trading to connect to kafka"
  type        = "egress"
  from_port   = 9092
  to_port     = 9092
  protocol    = "tcp"

  source_security_group_id = aws_security_group.MSK-SG.id
  security_group_id        = data.terraform_remote_state.infrastructure.outputs.common_sg_id
}

resource "aws_security_group_rule" "outbound_to_communicate_with_msk_zookeeper_from_real_trading" {
  description = "Rule which allows real-common-trading to connect to zookeeper"
  type        = "egress"
  from_port   = 2181
  to_port     = 2181
  protocol    = "tcp"

  source_security_group_id = aws_security_group.MSK-SG.id
  security_group_id        = data.terraform_remote_state.infrastructure.outputs.common_sg_id
}
