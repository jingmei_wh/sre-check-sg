/**
 * ## Overview
 *
 *  Common stack contains all common resources that should be applied in every region.
 *  This template will create MSK and security group
 *
 * ## Intro
 *
 *  Creates an MSK cluster.
 *
 *
 */

 resource "aws_cloudwatch_log_group" "broker_log" {
   name              = "/aws/msk/${local.msk_cluster_name}"
   retention_in_days = 30

   tags = merge(
     module.tags.map,
     { Name = "${var.product}-${var.channel}" }
   )
 }

resource "random_id" "configuration" {
  byte_length = 5
}

resource "aws_msk_configuration" "kafka_configuration" {
  kafka_versions = [var.kafka_version]
  name           = "trading-msk-${var.environment}-configuration-${random_id.configuration.hex}"
  description    = "Custom MSK Configuration"

  server_properties = <<PROPERTIES
 auto.create.topics.enable=false
 compression.type=producer
 default.replication.factor=1
 delete.topic.enable=true
 group.max.session.timeout.ms=30000
 group.min.session.timeout.ms=6000
 leader.imbalance.per.broker.percentage=10
 log.cleaner.delete.retention.ms=86400000
 log.cleaner.min.cleanable.ratio=0.5
 log.cleanup.policy=delete
 log.flush.interval.messages=9223372036854775807
 log.message.timestamp.type=CreateTime
 log.retention.bytes=-1
 log.retention.hours=168
 log.segment.bytes=1073741824
 message.max.bytes=1000012
 min.insync.replicas=2
 num.io.threads=8
 num.network.threads=3
 num.partitions=1
 num.recovery.threads.per.data.dir=1
 num.replica.fetchers=1
 offsets.retention.minutes=10080
 offsets.topic.replication.factor=3
 replica.fetch.max.bytes=1048576
 replica.lag.time.max.ms=10000
 replica.socket.receive.buffer.bytes=65536
 socket.receive.buffer.bytes=102400
 socket.request.max.bytes=104857600
 socket.send.buffer.bytes=102400
 unclean.leader.election.enable=false
 zookeeper.session.timeout.ms=6000
 PROPERTIES
}

resource "aws_msk_cluster" "kafka" {
  cluster_name           = local.msk_cluster_name
  kafka_version          = var.kafka_version
  number_of_broker_nodes = var.number_of_broker_nodes

  broker_node_group_info {
    instance_type   = var.kafka_instance_type
    client_subnets  = data.terraform_remote_state.infra.outputs.vpc_private_subnets
    security_groups = [data.terraform_remote_state.infra.outputs.common_sg_id, aws_security_group.MSK-SG.id, data.terraform_remote_state.infrastructure.outputs.common_sg_id]
    ebs_volume_size = var.kafka_ebs_volume_size
  }

  configuration_info {
    arn      = aws_msk_configuration.kafka_configuration.arn
    revision = aws_msk_configuration.kafka_configuration.latest_revision
  }

  encryption_info {
    encryption_in_transit {
      client_broker = "PLAINTEXT"
    }
  }

  open_monitoring {
    prometheus {
      jmx_exporter {
        enabled_in_broker = true
      }
      node_exporter {
        enabled_in_broker = true
      }
    }
  }

  enhanced_monitoring = "PER_BROKER"

  logging_info {
    broker_logs {
      cloudwatch_logs {
        enabled   = true
        log_group = aws_cloudwatch_log_group.broker_log.name
      }
    }
  }

  tags = merge(
    module.tags.map,
    { Name = "${var.product}-${var.channel}" }
  )

  depends_on = [aws_cloudwatch_log_group.broker_log, aws_msk_configuration.kafka_configuration]

}
