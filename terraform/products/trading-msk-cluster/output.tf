output "msk_cluster_arn" {
  description = "Amazon Resource Name (ARN) of the MSK cluster"
  value       = aws_msk_cluster.kafka.arn
}

output "zookeeper_connect_string" {
  description = "A comma separated list of one or more IP:port pairs to use to connect to the Apache Zookeeper cluster"
  value       = "aws_route53_record.DNS_record_zo[0].name:2181, aws_route53_record.DNS_record_zo[1].name:2181, aws_route53_record.DNS_record_zo[2].name:2181, aws_route53_record.DNS_record_zo[3].name:2181, aws_route53_record.DNS_record_zo[4].name:2181, aws_route53_record.DNS_record_zo[5].name:2181"
}

output "bootstrap_brokers" {
  description = "Plaintext connection host:port pairs"
  value       = "aws_route53_record.DNS_record_br[0].name:9092, aws_route53_record.DNS_record_br[1].name:9092, aws_route53_record.DNS_record_br[2].name:9092, aws_route53_record.DNS_record_br[3].name:9092, aws_route53_record.DNS_record_br[4].name:9092, aws_route53_record.DNS_record_br[5].name:9092"
}

output "bootstrap_brokers_tls" {
  description = "TLS connection host:port pairs"
  value       = aws_msk_cluster.kafka.bootstrap_brokers_tls
}

output "zookeeper_connect_string_array" {
  description = "A comma separated list of one or more IP:port pairs to use to connect to the Apache Zookeeper cluster"
  value       = formatlist("%s:2181", aws_route53_record.DNS_record_zo.*.name)
}

output "bootstrap_brokers_array" {
  description = "Plaintext connection host:port pairs"
  value       = formatlist("%s:9092", aws_route53_record.DNS_record_br.*.name)
}
