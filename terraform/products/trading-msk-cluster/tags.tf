locals {
  stack_tags = {
    Environment = var.environment
    Product     = var.stack
    AppRole     = "${var.product} for ${var.channel}"
    Version     = var.product_version
    CostCentre  = var.billingId
    Compliance  = "ncde"
    Owner       = "System Engeneers Team"
    Approval    = "Yes"
    Escalation  = "slack: trading-syseng"
    Tier        = "External"
    SubDivision = var.channel
  }

  additional_tags = {
    BillingID    = var.billingId
    TerraformUrl = var.git_url
  }
}

module "tags" {
  source          = "git::https://git.nonprod.williamhill.plc/terraform_modules/tf-aws-williamhill-tags.git?ref=3.1.0"
  required_tags   = local.stack_tags
  additional_tags = local.additional_tags
}
