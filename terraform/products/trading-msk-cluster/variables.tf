variable "number_of_broker_nodes" {
  description = "number of broker nodes in msk."
  type        = number
  default     = 3
}

variable "aws_region" {}

variable "product" {}

variable "stack" {}

variable "environment" {}

variable "org" {}

variable "channel" {}

variable "product_version" {}

locals {
  msk_cluster_name  = "${var.channel}-${var.product}-${var.environment}"
  broker_endpoint_suffix   = regex("\\..*", element(split(":", element(split(",", aws_msk_cluster.kafka.bootstrap_brokers), 0)), 0))
  zookeeper_endpoint_suffix   = regex("\\..*", element(split(":", element(split(",", aws_msk_cluster.kafka.zookeeper_connect_string), 0)), 0))
}

variable "billingId" {
  type    = string
  default = "f6ad4ebe-2616-4fe6-b1ff-c967fe9925f0"
}

variable "git_url" {
  type = string
}

variable "kafka_instance_type" {
  description = "Instance type for kafka."
  type        = string
  default     = "kafka.m5.xlarge"
}

variable "kafka_version" {
  description = "kafka version."
  type        = string
  default     = "2.4.1"
}

variable "kafka_ebs_volume_size" {
  description = "Storage size for kafka."
  type        = string
  default     = "600"
}

variable "tags" {
  type    = map(string)
  default = {}
}
