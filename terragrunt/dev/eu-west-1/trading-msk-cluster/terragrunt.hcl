# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "${get_parent_terragrunt_dir()}/../terraform/products/trading-msk-cluster"
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

inputs = {
  number_of_broker_nodes = "6"
  kafka_instance_type    = "kafka.m5.2xlarge"
  kafka_ebs_volume_size  = "700"
}
