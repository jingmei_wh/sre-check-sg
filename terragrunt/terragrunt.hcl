# ---------------------------------------------------------------------------------------------------------------------
# TERRAGRUNT CONFIGURATION
# Terragrunt is a thin wrapper for Terraform that provides extra tools for working with multiple Terraform modules,
# remote state, and locking: https://github.com/gruntwork-io/terragrunt
# ---------------------------------------------------------------------------------------------------------------------

# Locals

locals {
  dirs              = split("/", path_relative_to_include())
  env               = local.dirs[0]
  aws_region        = local.dirs[1]
  stack             = local.dirs[2]
  org               = "williamhill"
  channel           = "trading"
  product           = "msk"
}

# Configure Terragrunt to automatically store tfstate files in an S3 bucket
# we are using common bucket
# key is defined as: /PRODUCT/ENV/REGION/STACK/terraform.tfstate
remote_state {
  backend = "s3"

  config = {
    encrypt = true
    bucket = join("-", [
      lower(local.org),
      lower(local.channel),
      lower(local.product),
      lower(local.env),
      lower(local.aws_region),
      "tfstate",
    ])
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = local.aws_region
    dynamodb_table = "terraform-locks"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# GLOBAL PARAMETERS
# These variables apply to all configurations in this subfolder. These are automatically merged into the child
# `terragrunt.hcl` config via the include block.
# ---------------------------------------------------------------------------------------------------------------------


inputs = {
  org         = local.org
  product     = local.product
  channel     = local.channel
  environment = local.env
  stack       = local.stack
  aws_region  = local.aws_region
  remote_infra_bucket = join("-", [
    lower(local.org),
    lower(local.channel),
    lower(local.product),
    lower(local.env),
    lower(local.aws_region),
    "tfstate",
  ])
  remote_infra_key = "${lower(local.env)}-lower(local.product).tfstate"

  cluster_name = local.dirs[2]
  app_version = local.dirs[0]
}
